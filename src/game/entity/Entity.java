package game.entity;

import game.world.Position;

/**
 * Created by Dale on 24/01/2015.
 */
public class Entity {

    /*Used to hold this players current position*/
    private final Position position = new Position(0, 0);


    public Position getPosition() {
        return position;
    }


}
