package game.entity.combat;

/**
 * Created by Dale on 1/02/2015.
 */
public enum PoisonType {

    DEFAULT_POISON(4),
    STRONG_POISON(5),
    SUPER_POISON(6);

    private int maxDamage;
    PoisonType(int maxDamage){
        this.maxDamage = maxDamage;
    }

    public int getMaxDamage(){
        return maxDamage;
    }
}
