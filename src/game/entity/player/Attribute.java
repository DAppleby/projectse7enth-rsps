package game.entity.player;

/**
 * Created by Dale on 25/01/2015.
 * <p>
 * A generic container class that represents a single attribute and holds one value.
 */
public class Attribute<T> {

    private T valueOne = null;

    public Attribute(T valueOne) {
        this.valueOne = valueOne;
    }

    /**
     * Getter for property 'value'.
     *
     * @return Value for property 'value'.
     */
    public T getValue() {
        return valueOne;
    }

    /**
     * Setter for property 'value'.
     *
     * @param value Value to set for property 'value'.
     */
    public void setValue(T value) {
        this.valueOne = value;
    }
}
