package game.entity.player;


/**
 * Created by Dale on 24/01/2015.
 */
public interface PlayerListener {

    public abstract void isOutOfEnergy();

    public abstract void hasBeenKilled(Player killer);

    public abstract void isInWilderness();

    public abstract void hasKilled(Player deadEnemy);

    public abstract void isUnderAttack(Player enemy);

    public abstract void isInSafeZone();
}
