package game.entity.player;

import game.task.combat.PoisonTimerTask;
import game.task.combat.SafeZoneTimerTask;
import game.task.combat.TeleBlockTimerTask;

/**
 * Created by Dale on 25/01/2015.
 * <p>
 * A class made up of @class Attribute objects in which together both classes represent the attributes of a player.
 */
public class PlayerAttributes {
    private Attribute<Integer> runEnergy = new Attribute(100);
    private Attribute<Integer> playersKilled = new Attribute(0);
    private Attribute<Integer> pkPoints = new Attribute(0);
    private Attribute<Boolean> autoCasting = new Attribute(false);
    private Attribute<Integer> specialAttackPercentage = new Attribute(100);
    private Attribute<Integer> combatLevel = new Attribute(3);
    private Attribute<Integer> wildernessLevel = new Attribute(0);
    private Attribute<Integer> isNewPlayer = new Attribute(false);
    private Attribute<Short> playerRights = new Attribute(0);
    private Attribute<TeleBlockTimerTask> teleBlockTimerTask = null;
    private Attribute<PoisonTimerTask> poisonTimerTask = null;
    private Attribute<SafeZoneTimerTask> safeZoneTimerTask = null;


    /** Constructs a new PlayerAttributes. */
    public PlayerAttributes() {}

    /**
     * Getter for property 'runEnergy'.
     *
     * @return Value for property 'runEnergy'.
     */
    public Attribute getRunEnergy() {
        return runEnergy;
    }

    /**
     * Getter for property 'playersKilled'.
     *
     * @return Value for property 'playersKilled'.
     */
    public Attribute getPlayersKilled() {
        return playersKilled;
    }

    /**
     * Getter for property 'pkPoints'.
     *
     * @return Value for property 'pkPoints'.
     */
    public Attribute getPkPoints() {
        return pkPoints;
    }

    /**
     * Getter for property 'autoCasting'.
     *
     * @return Value for property 'autoCasting'.
     */
    public Attribute getAutoCasting() {
        return autoCasting;
    }

    /**
     * Getter for property 'specialAttackPercentage'.
     *
     * @return Value for property 'specialAttackPercentage'.
     */
    public Attribute getSpecialAttackPercentage() {
        return specialAttackPercentage;
    }

    /**
     * Getter for property 'combatLevel'.
     *
     * @return Value for property 'combatLevel'.
     */
    public Attribute getCombatLevel() {
        return combatLevel;
    }

    /**
     * Getter for property 'wildernessLevel'.
     *
     * @return Value for property 'wildernessLevel'.
     */
    public Attribute getWildernessLevel() {
        return wildernessLevel;
    }

    /**
     * Getter for property 'teleBlockTimerTask'.
     *
     * @return Value for property 'teleBlockTimerTask'.
     */
    public Attribute<TeleBlockTimerTask> getTeleBlockTimerTask() {
        return teleBlockTimerTask;
    }

    /**
     * Getter for property 'poisonTimerTask'.
     *
     * @return Value for property 'poisonTimerTask'.
     */
    public Attribute<PoisonTimerTask> getPoisonTimerTask() {
        return poisonTimerTask;
    }

    /**
     * Getter for property 'safeZoneTimerTask'.
     *
     * @return Value for property 'safeZoneTimerTask'.
     */
    public Attribute<SafeZoneTimerTask> getSafeZoneTimerTask() {
        return safeZoneTimerTask;
    }

    /**
     * Getter for property 'isNewPlayer'.
     *
     * @return Value for property 'isNewPlayer'.
     */
    public Attribute getIsNewPlayer() {
        return isNewPlayer;
    }

    /**
     * Getter for property 'playerRights'.
     *
     * @return Value for property 'playerRights'.
     */
    public Attribute getPlayerRights() {
        return playerRights;
    }



}
