package game.entity.player;


import game.entity.Entity;
import net.ClientSession;

import java.io.Serializable;

/**
 * Created by Dale on 22/01/2015.
 */
public class Player extends Entity implements Serializable {

    /*Used to hold this players client session*/
    private final ClientSession session;

    /*Used to hold the credentials of a player*/
    private final PlayerCredentials credentials;

    /*The attributes of a particular player*/
    private final PlayerAttributes playerAttributes;


    public Player(final ClientSession session, PlayerCredentials credentials, PlayerAttributes attributes) {
        super();
        this.credentials = credentials;
        this.session = session;
        this.playerAttributes = attributes;
    }

    public void attack(Player target) {
    }

    public boolean canAttack(Player target) {
        return false;
    }

    /**
     * Getter for property 'session'.
     *
     * @return Value for property 'session'.
     */
    public ClientSession getSession() {
        return session;
    }

    /**
     * Getter for property 'credentials'.
     *
     * @return Value for property 'credentials'.
     */
    public PlayerCredentials getCredentials() {
        return credentials;
    }

    /**
     * Getter for property 'playerAttributes'.
     *
     * @return Value for property 'playerAttributes'.
     */
    public PlayerAttributes getPlayerAttributes() {
        return playerAttributes;
    }

    public void sendMessage(String message){

    }

    public void inflictPoisonDamage(int damage){

    }


    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (credentials != null ? !credentials.equals(player.credentials) : player.credentials != null) return false;
        if (playerAttributes != null ? !playerAttributes.equals(player.playerAttributes) : player.playerAttributes != null)
            return false;
        if (session != null ? !session.equals(player.session) : player.session != null) return false;

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = session != null ? session.hashCode() : 0;
        result = 31 * result + (credentials != null ? credentials.hashCode() : 0);
        result = 31 * result + (playerAttributes != null ? playerAttributes.hashCode() : 0);
        return result;
    }
}
