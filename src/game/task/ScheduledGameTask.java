package game.task;

import java.util.concurrent.TimeUnit;

/**
 * Created by Dale on 31/01/2015.
 *
 * @class ScheduledGameTask
 * A class that represents a game task that is executed on the main game thread, or by the main game thread pool.Subclasses will be able
 * to implement a task with a fixed period between successive executions and an optional initial delay. This class allows tasks
 * do be run at a different tick rate than the GameEngine(100 ticks per second) yet still be executed by the same thread.*/

 public abstract class ScheduledGameTask implements Task {
    public abstract void execute();

    public abstract int getExecutionDelay();

    public abstract int getInitialDelay();

    public abstract TimeUnit getTimeUnit();
}
