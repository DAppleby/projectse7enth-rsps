package game.task;

/**
 * Created by Dale on 31/01/2015.
 */
public interface Task {
    public void execute();
}
