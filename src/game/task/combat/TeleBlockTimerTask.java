package game.task.combat;

import game.entity.player.Player;
import game.task.FixedTickGameTask;

/**
 * Created by Dale on 31/01/2015.
 */
public class TeleBlockTimerTask extends FixedTickGameTask {

    private final Player player;

    //The number of ticks to execute this task for (100 ticks per minute)
    public TeleBlockTimerTask(Player player){
        this.player = player;
        ticks = 500;
    }

    @Override
    public void execute() {
        player.getPlayerAttributes().getTeleBlockTimerTask().setValue(this);
    }

    @Override
    public void onFinish() {
        player.getPlayerAttributes().getTeleBlockTimerTask().setValue(null);
    }

}