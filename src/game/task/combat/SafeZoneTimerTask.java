package game.task.combat;

import game.entity.player.Player;
import game.task.FixedTickGameTask;

/**
 * Created by Dale on 1/02/2015.
 */
public class SafeZoneTimerTask extends FixedTickGameTask<Player> {
    private final Player player;

    public SafeZoneTimerTask(Player player){
        this.player = player;
        ticks = 9;
    }

    @Override
    public void execute() {
        player.getPlayerAttributes().getSafeZoneTimerTask().setValue(this);
    }

    @Override
    protected void onFinish() {
        player.getPlayerAttributes().getSafeZoneTimerTask().setValue(null);

    }
}
