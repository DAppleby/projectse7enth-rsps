package game.task.combat;

import game.entity.combat.PoisonType;
import game.entity.player.Player;
import game.task.FixedTickGameTask;

/**
 * Created by Dale on 1/02/2015.
 */
public class PoisonTimerTask extends FixedTickGameTask<Player> {
    private Player player;
    private int poisonTicks = 25;
    private int startTicks = 0;
    private int damageToInflict = 0;


    public PoisonTimerTask(Player player, PoisonType type){
        this.player = player;
        this.damageToInflict = type.getMaxDamage();
        this.startTicks = type.getMaxDamage() * 100;
        this.ticks = type.getMaxDamage() * 100;
    }

    @Override
    public void execute() {
        player.sendMessage("You have been poisoned!");
        if(getTicksUntilFinish() <= startTicks - poisonTicks){
            player.inflictPoisonDamage(damageToInflict);
            if(poisonTicks == 100){
                damageToInflict--;
                poisonTicks = 25;
            }
        }
        poisonTicks+=25;
    }

    @Override
    protected void onFinish() {player.sendMessage("The poison has worn off!");}
}
