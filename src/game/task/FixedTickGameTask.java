package game.task;


import java.util.concurrent.ScheduledFuture;

/*********************************************************************************************************************************
 * Created by Dale on 31/01/2015.
 * @class FixedTickGameTask<T>
 * A class that represents a game task that will run for a fixed number of ticks. The range of valid ticks is anywhere between
 * 0 and INTEGER.MAX_VALUE. If a subclass extends this class and changes the number of ticks to 0, and the task is then Scheduled
 * via @class GameEngine @method executeNewTask(Task task) the task will run until the GameEngine is shutdown,
 * or the user cancels it via the ScheduledFuture object returned upon invoking the method.
 **********************************************************************************************************************************/

public abstract class FixedTickGameTask<T> implements Task {
    /*The number of ticks this task should run for at any point if this is set to 0, it means that
    * the task will run until the GameEngine is shutdown or the task is manually cancelled.*/
    protected int ticks = 1;

    /*The scheduled future object assigned to this task enabling it to cancel itself*/
    private ScheduledFuture future = null;

    /*An optional attachment which could be later used within the onFinish method, to retrieve the assigned object
      invoke a particular method.
     */
    private T attachement = null;

    /*The code to be run or executed for the fixed amount of ticks*/
    public abstract void execute();

    /*Used to retrieve the attachment assigned to this task*/
    public final T getAttachement() {
        return attachement;
    }

    /*Sets the attachment for this task*/
    public final FixedTickGameTask setAttachement(T attachement) {
        this.attachement = attachement;
        return this;
    }
    /*Sets the scheduled future for this task*/
    public final void setScheduledFuture(ScheduledFuture future) {
        this.future = future;
    }

    /*Cancels this task assuming @member field future is set,this is done automatically when scheduling tasks via
    the @class GameEngine and should be done assuming tasks are extended to use other executor services,threads or thread pools.
     */
    public final boolean cancelTask() {
        if (future != null) {
            onFinish();
            future.cancel(true);
            return true;
        }
        return false;
    }
    /*Decreases the number of ticks this task has until it has completed*/
    public final void decrementTicks() {
        ticks--;
    }

    /*Returns the number of ticks still left for this task*/
    public final int getTicksUntilFinish() {
        return ticks;
    }

    /*Sets the number of ticks for this task, convenience method incase using an anonymous class*/
    public final FixedTickGameTask setNumberOfTicks(int ticks) {
        this.ticks = ticks;
        return this;
    }
    /*A method that is called automatically via @class GameEngine when tasks are scheduled via the main game thread
    this can be overridden by subclasses and is called upon the termination of a task.
     */
    protected abstract void onFinish();
}
