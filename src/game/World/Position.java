package game.world;

/**
 * Created by Dale on 21/01/2015.
 */
public class Position {

    double x, y, z;

    private Position() {
    }

    public Position(double x, double y) {
        this(x, y, 0);
    }

    public Position(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Position distanceTo(Position pos) {
        return new Position((pos.getX() - this.getX()),
                (pos.getY() - this.getY()));
    }

    public Position shortestDistanceTo(Area area) {
        return new Position(Math.min(Math.sqrt(Math.pow(area.getNeX() - this.getX(), 2)),
                Math.sqrt(Math.pow(area.getSwX() - this.getX(), 2))),
                Math.min(Math.sqrt(Math.pow(area.getNeY() - this.getY(), 2)),
                        Math.sqrt(Math.pow(area.getSwY() - this.getY(), 2))));
    }

    public Position averageDistanceTo(Area area) {
        return new Position(0, 0);
    }

    public Position longestDistanceTo(Area area) {
        return new Position(Math.max(Math.sqrt(Math.pow(area.getNeX() - this.getX(), 2)),
                Math.sqrt(Math.pow(area.getSwX() - this.getX(), 2))),
                Math.max(Math.sqrt(Math.pow(area.getNeY() - this.getY(), 2)),
                        Math.sqrt(Math.pow(area.getSwY() - this.getY(), 2))));
    }

    public boolean canView(Position pos) {
        return false;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(this.getX(), 2) + Math.pow(this.getY(), 2));
    }

    public boolean withinArea(Area area) {
        return area.containsPosition(this);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
