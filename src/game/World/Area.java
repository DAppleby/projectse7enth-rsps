package game.world;

/**
 * Created by Dale on 21/01/2015.
 * <p>
 * A class that represents an area within the game.
 */
public class Area {

    private int swX = 0, swY = 0, neX = 0, neY = 0, centerX = 0, centerY = 0;

    public Area() {
        swX = swY = neX = neY = 0;
    }

    public Area(int swX, int swY, int neX, int neY) {
        this.swX = swX;
        this.swY = swY;
        this.neX = neX;
        this.neY = neY;
        this.centerX = (neX - swX) / 2;
        this.centerY = (neY - neY) / 2;

    }

    public Area(int centerX, int centerY, int radius) {
        this(centerX - radius, centerY - radius, centerX + radius, centerY + radius);
    }

    public static void main(String[] args) {


    }

    public boolean containsPosition(Position position) {
        return (position.getX() <= this.neX && position.getX() >= this.swX)
                && (position.getY() <= this.neY && position.getX() >= this.swX);
    }

    public int getSwX() {
        return swX;
    }

    public void setSwX(int swX) {
        this.swX = swX;
    }

    public int getSwY() {
        return swY;
    }

    public void setSwY(int swY) {
        this.swY = swY;
    }

    public int getNeX() {
        return neX;
    }

    public void setNeX(int neX) {
        this.neX = neX;
    }

    public int getNeY() {
        return neY;
    }

    public void setNeY(int neY) {
        this.neY = neY;
    }

    public int getCenterX() {
        return centerX;
    }

    public int getCenterY() {
        return centerY;
    }


}
