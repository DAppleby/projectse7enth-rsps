package game;

import game.world.World;
import game.task.FixedTickGameTask;
import game.task.ScheduledGameTask;
import game.task.Task;

import java.util.concurrent.*;

/**
 * Created by Dale on 24/01/2015.
 *
 * @class GameEngine
 * A class which represents a game engine, using a scheduled executor service to continuously execute Scheduled tasks.
 * The game engine will be run by two threads if the hosting computer has more than one processor, else the GameEngine will be
 * run by one thread.
 *
 * Tasks can be executed by the same thread, or thread pool by extending @FixedTickGameTask or @ScheduledGameTask
 * or a class implementing the task interface and then calling @method public ScheduledFuture executeNewTask(Task task)
 */

public class GameEngine implements Runnable {
    private ScheduledExecutorService gameServer = null;
    private static GameEngine gameEngine = null;
    private World gameWorld = new World();

    private GameEngine() {
    }

    {
        if(Runtime.getRuntime().availableProcessors() > 1){
            gameServer = Executors.newScheduledThreadPool(2);
        }else{
            gameServer = Executors.newSingleThreadScheduledExecutor();
        }
        gameServer.scheduleAtFixedRate(() -> gameEngine.run(), 0, 600, TimeUnit.MILLISECONDS);
    }

    public static GameEngine getInstance() {
        if (gameEngine != null) {
            return gameEngine;
        }
        return gameEngine = new GameEngine();
    }


    public ScheduledFuture executeNewTask(Task task) {
        ScheduledFuture future = null;
        if (task instanceof ScheduledGameTask) {
            future = gameServer.scheduleAtFixedRate(
                    () -> task.execute(),
                        ((ScheduledGameTask) task).getInitialDelay(),
                        ((ScheduledGameTask) task).getExecutionDelay(),
                        ((ScheduledGameTask) task).getTimeUnit());

        } else if (task instanceof FixedTickGameTask) {

            FixedTickGameTask fixedTask = (FixedTickGameTask) task;

            boolean runIndefinitely = fixedTask.getTicksUntilFinish() == 0;

            future = gameServer.scheduleAtFixedRate(() -> {
                        try {
                            if(runIndefinitely){
                                fixedTask.execute();
                            }
                            else if (fixedTask.getTicksUntilFinish() > 0 && !runIndefinitely) {
                                fixedTask.execute();
                                fixedTask.decrementTicks();
                                if(fixedTask.getTicksUntilFinish() == 0){
                                    fixedTask.cancelTask();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }, 0, 600, TimeUnit.MILLISECONDS);

             //Required in order to successfully cancel a FixedTickGameTask
            fixedTask.setScheduledFuture(future);

        }else{
            future = gameServer.scheduleAtFixedRate(() -> task.execute(),
                    0,600,TimeUnit.MILLISECONDS);
        }
        return future;
    }

    public void shutdown() {
        if (!gameServer.isShutdown()) {
            gameServer.shutdown();
        }
        try {
            if (!gameServer.isShutdown()) {
                gameServer.awaitTermination(5000, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            gameServer.shutdownNow();
            e.printStackTrace();
        }
    }

    @Override
    public void run() {

    }
}
