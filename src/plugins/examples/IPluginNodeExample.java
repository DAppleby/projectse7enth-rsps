package plugins.examples;

import game.GameEngine;
import plugins.api.IPluginNode;
import plugins.config.PluginConfiguration;

/**
 * Created by Dale on 24/01/2015.
 * <p>
 * An interface which should be implemented in order to create plugins.
 */
public class IPluginNodeExample implements IPluginNode {

    private PluginConfiguration configuration;

    @Override
    public void init(PluginConfiguration pluginConfiguration) {
        this.configuration = pluginConfiguration;
    }

    @Override
    public void poll(GameEngine engine) {}

    @Override
    public void stop() {}

    //Optional
    @Override
    public void dispose() {

    }
}
