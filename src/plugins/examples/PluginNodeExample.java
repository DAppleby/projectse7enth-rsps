package plugins.examples;

import game.GameEngine;
import plugins.api.PluginNode;
import plugins.config.PluginConfiguration;


/**
 * **********************************************************************
 *
 * @class PluginNodeExample                                              *
 * An example of a plugin that extends the abstract class @PluginNode    *
 * First the init method is called, allowing the plugin writer to set    *
 * the plugins configuration settings. Once complete, poll is called and *
 * looped over until a) pluginIsRunning is set to false or b)            *
 * the stop method is called from within poll by the Plugin writer.      *
 * ***********************************************************************
 */

public class PluginNodeExample extends PluginNode {


    @Override
    public void init(PluginConfiguration pluginConfiguration) {
        pluginConfiguration.setName("hi"); //Set the plugins name
        pluginConfiguration.setNumberOfTicks(4); //Set how long the plugin should run
    }

    @Override
    public void poll(GameEngine engine) {
        System.out.println("hi from abstract class extension Plguin Node");
    }


    @Override
    public void stop() {

    }


}
