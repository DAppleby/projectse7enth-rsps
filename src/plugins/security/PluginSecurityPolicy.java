package plugins.security;

import plugins.api.PluginManager;

import java.security.*;

/**
 * Created by Dale on 24/01/2015.
 */

public class PluginSecurityPolicy extends Policy {

    @Override
    public PermissionCollection getPermissions(ProtectionDomain domain) {
        if (!domainIsPlugin(domain)) {
            return serverPermissions();
        }
        return pluginPermissions();
    }

    private boolean domainIsPlugin(ProtectionDomain domain) {
        return domain.getClassLoader() instanceof PluginManager;
    }

    private PermissionCollection pluginPermissions() {
        Permissions permissions = new Permissions(); // No permissions
        permissions.add(new RuntimePermission("accessDeclaredMembers"));
        return permissions;
    }

    private PermissionCollection serverPermissions() {
        Permissions permissions = new Permissions();
        permissions.add(new AllPermission());
        return permissions;
    }

}

