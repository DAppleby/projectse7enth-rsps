package plugins.api;

import game.GameEngine;
import game.task.FixedTickGameTask;
import plugins.config.PluginConfiguration;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Dale on 24/01/2015.
 * <p>
 * @class PluginManager
 * A class which is responsible for handling the loading of all plugins. The class will find and load any .class files within
 * the directory specified by the static final string PLUGINS_DIR. The class will then check to see that the class extends
 * the abstract @class PluginNode or implements the @interface IPluginNode. Upon successfully finding a plugin, the class is loaded
 * into the JVM and a new instance is created and it's poll method continuously executed, up until either a) Its specified number of ticks
 * have been reached as set in each plugins @PluginConfiguration or the plugin specifically sets itself running to be false.
 *
 * This class extends URLClassLoader enabling plugin code to run under its own protection domain, so security and policies for
 * each plugin can be implemented to sandbox running plugin code preventing malicious code from running.
 */
public class PluginManager extends URLClassLoader {

    /*The directory that holds the the .class file plugins. These classes must be packaged under the name "plugins"
     In order to be recognized.
     */
    public static final String PLUGINS_DIR = "C:/Users/Dale/Desktop/RSPS/ProjectSe7enth/out/production/ProjectSe7enth/plugins/examples";

    /*The logger for this class*/
    public static Logger logger = Logger.getLogger(PluginManager.class.getName());

    /*HashMap to store the plugins that extend the abstract class @PluginNode and the PluginConfiguration
     passed to the init method.
     */
    private final HashMap<PluginNode, PluginConfiguration> extendedPlugins = new HashMap<>();

    /*HashMap to store the plugins that implement the interface @IPluginInterface and the PluginConfiguration
     passed to the init method.
     */
    private final HashMap<IPluginNode, PluginConfiguration> implementedPlugins = new HashMap<>();


    private final URL[] pluginsDirectory;

    public PluginManager(URL[] urls) {
        super(urls);
        this.pluginsDirectory = urls;
    }


     /*
     * @Method loadAllPlugins()
     * @Description Static method responsible for loading all plugins and then submitting them to the executor service
     */

    public void loadAllPlugins() throws MalformedURLException {


        File file = new File(PLUGINS_DIR);

        if (!file.exists()) {
            logger.log(Level.INFO, "Plugins folder was not found, creating it now.");
            file.mkdir();
        }

        if (!file.canRead()) {
            logger.log(Level.INFO, "Cannot read directory plugins, attempting to modify permissions.");
            file.setReadable(true);
            file.setExecutable(true);
        }

        Arrays.stream(file.listFiles()).forEach(e -> {
            try {
                PluginConfiguration configuration = PluginConfiguration.PLUGIN_SETTINS.getPluginSettins();
                if (e.getName().endsWith(".class")) {

                    String pluginName = e.getName().replace(".class", "");
                    Class<?> aClass = loadClass("plugins.examples." + pluginName);

                    if (aClass.getSuperclass().getSimpleName().equals("PluginNode")) {
                        logger.log(Level.INFO, "Loading plugin - " + pluginName);

                        PluginNode plugin = (PluginNode) aClass.newInstance();

                        plugin.init(configuration);

                        FixedTickGameTask fixedTickGameTask = new FixedTickGameTask(){
                            @Override
                            public void execute() {
                                if(plugin.pluginIsRunning) {
                                    plugin.run();
                                }else{
                                    cancelTask();
                                }
                            }
                            @Override
                            public void onFinish() {
                                plugin.stop();
                                plugin.dispose();
                                logger.log(Level.INFO, "Plugin " + pluginName + " has finished running");
                            }
                        }.setNumberOfTicks(configuration.getNumberOfTicks());

                        GameEngine.getInstance().executeNewTask(fixedTickGameTask);

                        extendedPlugins.put(plugin, configuration);

                        logger.log(Level.INFO, "Successfully loaded extended plugin: " + pluginName);

                    } else if (Arrays.stream(aClass.getInterfaces()).anyMatch(b -> b.getSimpleName().equals("IPluginNode"))) {
                        IPluginNode plugin = (IPluginNode) aClass.newInstance();

                        plugin.init(configuration);

                        implementedPlugins.put(plugin, configuration);

                        GameEngine.getInstance().executeNewTask(new FixedTickGameTask() {
                            @Override
                            public void execute() {
                                if(configuration.isRunning()) {
                                    plugin.poll(GameEngine.getInstance());
                                }else{
                                    this.setNumberOfTicks(0);
                                    cancelTask();
                                }
                            }
                            @Override
                            protected void onFinish() {
                                plugin.stop();
                                plugin.dispose();
                                logger.log(Level.INFO, "Plugin " + pluginName + " has finished running");
                            }
                        }.setNumberOfTicks(configuration.getNumberOfTicks()));
                        logger.log(Level.INFO, "Successfully loaded implemented plugin: " + pluginName);
                    }
                }
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (InstantiationException e1) {
                e1.printStackTrace();
            } catch (IllegalAccessException e1) {
                e1.printStackTrace();
            }
        });
    }
}


