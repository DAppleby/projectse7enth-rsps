package plugins.api;

import game.GameEngine;
import plugins.config.PluginConfiguration;

/**
 * Created by SeventhSense on 24/01/2015.
 * An interface which can be implemented to in order to create plugins.
 */

public interface IPluginNode {

    public abstract void init(PluginConfiguration pluginConfiguration);

    public abstract void poll(GameEngine engine);

    abstract void stop();

    default void dispose() {
    }
}
