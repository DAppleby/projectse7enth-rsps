package plugins.api;

import game.GameEngine;
import plugins.config.PluginConfiguration;

/**
 * Created by Dale on 24/01/2015.
 * <p>
 * An abstract class which can be extended to create plugins.
 */
public abstract class PluginNode {

    protected boolean pluginIsRunning = true;

    public abstract void init(PluginConfiguration pluginConfiguration);

    protected final void run() {
        poll(GameEngine.getInstance());
    }

    protected abstract void poll(GameEngine engine);

    protected abstract void stop();

    protected void dispose() {
    }
}
