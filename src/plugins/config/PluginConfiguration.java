package plugins.config;

/**
 * Created by Dale on 24/01/2015.
 * <p>
 * An enum used to hold the configuration settings for a particular plugin.
 * Default constants are set -> should the plugin writer choose not to declare any configuration settings.
 */
public enum PluginConfiguration {

    PLUGIN_SETTINS("Plugin", true,0);

    private String value;
    private boolean isRunning;
    private int numberOfTicks;

    PluginConfiguration(String value, boolean isRunning, int numberOfTicks) {
        this.value = value;
        this.numberOfTicks = numberOfTicks;
        this.isRunning = isRunning;
    }

    public void setName(String value) {
        this.value = value;
    }

    public int getNumberOfTicks() {
        return numberOfTicks;
    }

    public void setNumberOfTicks(int ticks){this.numberOfTicks = ticks;}

    public String getValue() {
        return value;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void setRunning(boolean isRunning) {
        this.isRunning = isRunning;
    }

    public PluginConfiguration getPluginSettins() {
        return this;
    }

}
