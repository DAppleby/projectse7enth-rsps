package util;

import java.nio.*;

/**
 * Created by Dale on 28/01/2015.
 */
public class ExtendedBuffer {

    public enum Mode{
        BIG,LITTLE
    }

    private Mode mode = null;
    private ByteBuffer buffer;

    public ExtendedBuffer(int capacity, Mode mode){
        this.buffer = ByteBuffer.allocateDirect(capacity);
        this.mode = mode;
    }

    public void checkCapacity(int requested){
        if(buffer.position() + requested < buffer.capacity()){
            return;
        }
        ByteBuffer dupBuffer = ByteBuffer.allocateDirect(this.buffer.capacity() * 2);
        dupBuffer.put(this.buffer);
        dupBuffer.flip();
        this.buffer = dupBuffer;
        System.out.println("Output buffer has doubled in size");
    }


    public void writeByte(int byteValue){
        checkCapacity(1);
        buffer.put((byte) byteValue);
    }

    public void writeInt(int value){
        switch (mode){
            case BIG:
                writeByte(value >> 24);
                writeByte(value >> 16);
                writeByte(value >> 8);
                writeByte(value);
                break;
            case LITTLE:
                writeByte((byte) ((value) & 0xFF));
                writeByte((byte) ((value >> 8) & 0xFF));
                writeByte((byte) ((value >> 16) & 0xFF));
                writeByte((byte) ((value >> 24) & 0xFF));
                break;
        }
    }

    public ByteBuffer getBackingBuffer(){
        return this.buffer;
    }


    public void writeLong(long longValue){
        switch (mode){
            case BIG:
                for(int i = 56; i >= 0; i-=8){
                    writeByte((byte) ((longValue >> i) & 0xFF));
                }
                break;
            case LITTLE:
                for(int i = 0; i <= 56; i+=8) {
                    writeByte((byte) ((longValue >> i) & 0xFF));
                }
                break;
        }
    }

    public void writeShort(short shortValue){
        switch (mode){
            case BIG:
                writeByte(((byte) (shortValue >> 8)));
                writeByte((byte) shortValue);
                break;
            case LITTLE:
                writeByte((byte) shortValue);
                writeByte(((byte) (shortValue >> 8)));
                break;
        }
    }

    public int getInt(){
        int value = 0;
        switch (mode){
            case BIG:
                value = (int) this.buffer.get() << 24 | (int) this.buffer.get() << 16 |
                        (int) this.buffer.get() << 8 | (int) this.buffer.get();
                break;
            case LITTLE:
                value =  (int) this.buffer.get() |  (int) this.buffer.get() << 8 |
                        (int) this.buffer.get() << 16 |  (int) this.buffer.get() << 24;
                break;
        }
        return value;
    }

    public long getLong(){
        long value = 0L;
        switch (mode){
            case BIG:
                value |= buffer.get() << 56 | buffer.get() << 48 | buffer.get() << 40 | buffer.get() << 32
                | buffer.get() << 24 | buffer.get() << 16 | buffer.get() << 8| buffer.get();
                break;
            case LITTLE:
                break;
        }
        return value;
    }









}
