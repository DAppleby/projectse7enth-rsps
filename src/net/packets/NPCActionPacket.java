package net.packets;

import java.nio.channels.SelectionKey;

/**
 * Created by Dale on 2/02/2015.
 */
public class NPCActionPacket implements IPacketImpl {
    private final int NPC_ACTION_TWO = 17;
    private final int NPC_ACTION_THREE = 21;
    private final int NPC_ACTION_FOUR = 18;
    private final int ATTACK_NPC = 72;


    @Override
    public void handle(SelectionKey key, int opCode) {
        switch (opCode){
            case NPC_ACTION_TWO:
                break;
            case NPC_ACTION_THREE:
                break;
            case NPC_ACTION_FOUR:
                break;
            case ATTACK_NPC:
                break;
        }

    }

    @Override
    public int[] getOpCode() {
        return new int[]{
                NPC_ACTION_TWO,
                NPC_ACTION_THREE,
                NPC_ACTION_FOUR,
                ATTACK_NPC
        };
    }
}
