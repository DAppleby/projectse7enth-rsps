/* Copyright (c) 2009 Graham Edgecombe, Blake Beaupain and Brett Russell
 *
 * More information about Hyperion may be found on this website:
 *    http://hyperion.grahamedgecombe.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package net.packets.enc;

/**
 * <p>
 * An implementation of an ISAAC cipher. See <a
 * href="http://en.wikipedia.org/wiki/ISAAC_(cipher)">
 * http://en.wikipedia.org/wiki/ISAAC_(cipher)</a> for more information.
 * </p>
 * <p>
 * <p>
 * This implementation is based on the one written by Bob Jenkins, which is
 * available at <a href="http://www.burtleburtle.net/bob/java/rand/Rand.java">
 * http://www.burtleburtle.net/bob/java/rand/Rand.java</a>.
 * </p>
 *
 * @author Graham Edgecombe
 */
public class ISAACCipher {

    /**
     * The golden ratio.
     */
    public static final int RATIO = 0x9e3779b9;

    /**
     * The log of the size of the results and memory arrays.
     */
    public static final int SIZE_LOG = 8;

    /**
     * The size of the results and memory arrays.
     */
    public static final int SIZE = 1 << SIZE_LOG;

    /**
     * For pseudorandom lookup.
     */
    public static final int MASK = (SIZE - 1) << 2;
    /**
     * The results.
     */
    private final int results[] = new int[SIZE];
    /**
     * The internal memory state.
     */
    private final int memory[] = new int[SIZE];
    /**
     * The count through the results.
     */
    private int count = 0;
    /**
     * The accumulator.
     */
    private int a;

    /**
     * The last result.
     */
    private int b;

    /**
     * The counter.
     */
    private int c;

    /**
     * Creates the ISAAC cipher.
     *
     * @param seed The seed.
     */
    public ISAACCipher(final int[] seed) {
        for (int i = 0; i < seed.length; i++) {
            this.results[i] = seed[i];
        }
        this.init(true);
    }

    /**
     * Gets the next value.
     *
     * @return The next value.
     */
    public int getKey() {
        if (this.count-- == 0) {
            this.isaac();
            this.count = SIZE - 1;
        }
        return this.results[this.count];
    }

    /**
     * Generates 256 results.
     */
    public void isaac() {
        int i, j, x, y;
        this.b += ++this.c;
        for (i = 0, j = SIZE / 2; i < (SIZE / 2); ) {
            x = this.memory[i];
            this.a ^= this.a << 13;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a >>> 6;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a << 2;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a >>> 16;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;
        }
        for (j = 0; j < (SIZE / 2); ) {
            x = this.memory[i];
            this.a ^= this.a << 13;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a >>> 6;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a << 2;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;

            x = this.memory[i];
            this.a ^= this.a >>> 16;
            this.a += this.memory[j++];
            this.memory[i] = y = this.memory[(x & MASK) >> 2] + this.a + this.b;
            this.results[i++] = this.b = this.memory[((y >> SIZE_LOG) & MASK) >> 2] + x;
        }
    }

    /**
     * Initializes the ISAAC.
     *
     * @param flag Flag indicating if we should perform a second pass.
     */
    public void init(final boolean flag) {
        int i;
        int a, b, c, d, e, f, g, h;
        a = b = c = d = e = f = g = h = RATIO;
        for (i = 0; i < 4; ++i) {
            a ^= b << 11;
            d += a;
            b += c;
            b ^= c >>> 2;
            e += b;
            c += d;
            c ^= d << 8;
            f += c;
            d += e;
            d ^= e >>> 16;
            g += d;
            e += f;
            e ^= f << 10;
            h += e;
            f += g;
            f ^= g >>> 4;
            a += f;
            g += h;
            g ^= h << 8;
            b += g;
            h += a;
            h ^= a >>> 9;
            c += h;
            a += b;
        }
        for (i = 0; i < SIZE; i += 8) {
            if (flag) {
                a += this.results[i];
                b += this.results[i + 1];
                c += this.results[i + 2];
                d += this.results[i + 3];
                e += this.results[i + 4];
                f += this.results[i + 5];
                g += this.results[i + 6];
                h += this.results[i + 7];
            }
            a ^= b << 11;
            d += a;
            b += c;
            b ^= c >>> 2;
            e += b;
            c += d;
            c ^= d << 8;
            f += c;
            d += e;
            d ^= e >>> 16;
            g += d;
            e += f;
            e ^= f << 10;
            h += e;
            f += g;
            f ^= g >>> 4;
            a += f;
            g += h;
            g ^= h << 8;
            b += g;
            h += a;
            h ^= a >>> 9;
            c += h;
            a += b;
            this.memory[i] = a;
            this.memory[i + 1] = b;
            this.memory[i + 2] = c;
            this.memory[i + 3] = d;
            this.memory[i + 4] = e;
            this.memory[i + 5] = f;
            this.memory[i + 6] = g;
            this.memory[i + 7] = h;
        }
        if (flag) {
            for (i = 0; i < SIZE; i += 8) {
                a += this.memory[i];
                b += this.memory[i + 1];
                c += this.memory[i + 2];
                d += this.memory[i + 3];
                e += this.memory[i + 4];
                f += this.memory[i + 5];
                g += this.memory[i + 6];
                h += this.memory[i + 7];
                a ^= b << 11;
                d += a;
                b += c;
                b ^= c >>> 2;
                e += b;
                c += d;
                c ^= d << 8;
                f += c;
                d += e;
                d ^= e >>> 16;
                g += d;
                e += f;
                e ^= f << 10;
                h += e;
                f += g;
                f ^= g >>> 4;
                a += f;
                g += h;
                g ^= h << 8;
                b += g;
                h += a;
                h ^= a >>> 9;
                c += h;
                a += b;
                this.memory[i] = a;
                this.memory[i + 1] = b;
                this.memory[i + 2] = c;
                this.memory[i + 3] = d;
                this.memory[i + 4] = e;
                this.memory[i + 5] = f;
                this.memory[i + 6] = g;
                this.memory[i + 7] = h;
            }
        }
        this.isaac();
        this.count = SIZE;
    }
}