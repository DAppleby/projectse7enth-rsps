package net.packets;

import net.packets.IPacketImpl;

import java.nio.channels.SelectionKey;

/**
 * Created by Dale on 2/02/2015.
 */
public class ItemClickPacket implements IPacketImpl {
    private final int ITEM_ON_PLAYER = 14;
    private final int ITEM_ON_FLOOR_ITEM = 25;
    private final int ITEM_ON_NPC = 57;
    private final int ITEM_ON_ITEM = 53;
    private final int ITEM_ON_OBJECT = 192;

    @Override
    public void handle(SelectionKey key, int opCode) {

    }

    @Override
    public int[] getOpCode() {
        return new int[]{
                ITEM_ON_PLAYER,
                ITEM_ON_FLOOR_ITEM,
                ITEM_ON_NPC,
                ITEM_ON_ITEM,
                ITEM_ON_OBJECT
        };
    }
}
