package net.packets;

import java.nio.channels.SelectionKey;

/**
 * Created by Dale on 23/01/2015.
 */
public interface IPacketImpl {
    public void handle(SelectionKey key, int opCode);

    public int[] getOpCode();
}
