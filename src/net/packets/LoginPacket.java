package net.packets;

import game.entity.player.Player;
import net.ClientSession;
import net.packets.enc.ISAACCipher;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by Dale on 23/01/2015.
 */
public class LoginPacket implements IPacketImpl {
    public static final Logger logger = Logger.getLogger(LoginPacket.class.getName());
    private static final BigInteger RSA_MODULUS = new BigInteger("132116866569903165832989082319871177504226169558817284221217968800636718431988750688883965049483839048219201594845998962657395577201531726548162958247577649274518660168588391868283214703877747137478764209978855561442502045158783430738318074852146637984045202262925114691986907701163746690962982993177716359117");
    private static final BigInteger RSA_EXPONENT = new BigInteger("32250570385969923051042301889822529223379316425865677601523491537186417160916063193627503133522170027517444910727471373492729518349483567470551154402013318656564141724705871770290787384128924028017601568824153589440402847510640912333228936524750720514214896201979298115740937554338579336873996222882012983297");
    private final short INITIAL_REQUEST = 14;
    private final short LOGIN_REQUEST = 16;
    private final short LOGIN_REQUEST_TYPE_2 = 18;

    @Override
    public void handle(SelectionKey key, int opcode) {
        SocketChannel channel = (SocketChannel) key.channel();
        ClientSession session = (ClientSession) key.attachment();

        switch (opcode) {
            case INITIAL_REQUEST:
                session.getOutBuffer().putLong(0); //Ignored long by client
                session.getOutBuffer().put((byte) 0x0); //0 to confirm login to client
                session.getOutBuffer().putLong(new SecureRandom().nextLong()); //SSK
                session.writeOutBuffer();
                break;
            case LOGIN_REQUEST:

                if (session.getInBuffer().remaining() < 2) {
                    session.getInBuffer().compact();
                    return;
                }

                // Validate the login type.
                int loginType = session.getInBuffer().get();

                if (loginType != 16 && loginType != 18) {
                    logger.warning("Invalid login type: " + loginType);
                    session.disconnect();
                    return;
                }

                // Ensure that we can read all of the login block.
                int blockLength = session.getInBuffer().get() & 0xff;
                System.out.println("blockLength = " + blockLength);
                int loginEncryptPacketSize = blockLength - (36 + 1 + 1 + 2);
                System.out.println("loginEncryptPacketSize = " + loginEncryptPacketSize);


                if (loginEncryptPacketSize <= 0) {
                    logger.warning("Zero RSA packet size");
                    session.disconnect();
                    return;
                }

                if (session.getInBuffer().remaining() < blockLength) {
                    session.getInBuffer().flip();
                    session.getInBuffer().compact();
                    return;
                }


                System.out.println(session.getInBuffer().get()); // Ignore the magic ID.


                // Validate the client version.
                int clientVersion = session.getInBuffer().getShort();
                System.out.println(clientVersion);

                if (clientVersion != 317) {
                    logger.warning("Invalid client version: " + clientVersion);
                    session.disconnect();
                    return;
                }

                System.out.println(session.getInBuffer().get()); // Skip the high/low memory version.


                for (int i = 0; i < 9; i++) { // Skip the CRC keys.
                    System.out.println(session.getInBuffer().getInt());
                }
                loginEncryptPacketSize--;
                session.getInBuffer().get();

                String username = null;
                String password = null;



                    // Create the RSA buffer.
                    byte[] encryptionBytes = new byte[loginEncryptPacketSize];
                    session.getInBuffer().get(encryptionBytes);

                    ByteBuffer rsaBuffer = ByteBuffer.wrap(new BigInteger(
                            encryptionBytes).modPow(RSA_EXPONENT, RSA_MODULUS).toByteArray());

                    // Check if RSA block can be decoded.
                    int rsaOpcode = rsaBuffer.get();

                    if (rsaOpcode != 10) {
                        logger.warning("Unable to decode RSA block properly!");
                        session.disconnect();
                        return;
                    }

                    // Set up the ISAAC ciphers.
                    long clientHalf = rsaBuffer.getLong();
                    long serverHalf = rsaBuffer.getLong();

                    int[] isaacSeed = {(int) (clientHalf >> 32), (int) clientHalf,
                            (int) (serverHalf >> 32), (int) serverHalf};

                    ISAACCipher decryptor = new ISAACCipher(isaacSeed);

                    for (int i = 0; i < isaacSeed.length; i++) {
                        isaacSeed[i] += 50;

                    }

                    ISAACCipher encryptor = new ISAACCipher(isaacSeed);

                    // Read the user authentication.
                    rsaBuffer.getInt(); // Skip the user ID.;
                    username = readString(rsaBuffer);
                    password = readString(rsaBuffer);

                    if(authenticate(username, password)){
                        Player player = session.getPlayer();
                        player.getCredentials().setUsername(username);
                        player.getCredentials().setPassword(password);
                        session.getOutBuffer().put((byte)2);
                        session.getOutBuffer().put((byte)3);
                        session.getOutBuffer().put((byte)0);
                        session.writeOutBuffer();
                    }

                //Finish login

                break;
        }
    }

    private final boolean authenticate(String username,String password){
        URL url = null;
        try {
            url = new URL("http://www.pandorium-rsps.com/authenticate.php");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if(url != null){

            try {
               HttpURLConnection con  = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");
                con.addRequestProperty("username",username);
                con.addRequestProperty("password",password);
                con.connect();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return true;
    }

    public String readString(ByteBuffer buffer) {
        byte temp;
        StringBuilder b = new StringBuilder();
        while ((temp = buffer.get()) != 10) {
            b.append((char) temp);
        }
        return b.toString();
    }

    @Override
    public int[] getOpCode() {
        return new int[]{
                INITIAL_REQUEST,
                LOGIN_REQUEST,
                LOGIN_REQUEST_TYPE_2
        };
    }
}
