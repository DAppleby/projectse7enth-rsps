package net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Dale on 22/01/2015.
 */
public class ServerEngine implements Runnable {

    public static Logger logger = Logger.getLogger(ServerEngine.class.getName());
    private static SelectionKey lastRemovedKeyTracker = null;
    private final InetSocketAddress inetSocketAddress;
    private final int bindPort;
    private ServerSocketChannel serverSocketChannel;
    private boolean serverRunning = false;
    private Thread serverThread = null;
    private Selector selector = null;

    /*Constructor for the server engine*/
    public ServerEngine(InetSocketAddress inetSocketAddress) {
        this.inetSocketAddress = inetSocketAddress;
        this.bindPort = inetSocketAddress.getPort();
        if (bindPort > 0 && bindPort <= 1024) {
            throw new InstantiationError("Bind port should not be between 1 and 1024 inclusive");
        }
    }

    /*Starts the server*/
    public final void start() {
        if (serverRunning) {
            return;
        }
        try {
            logger.log(Level.INFO, "Server starting: listening on ip "
                    + inetSocketAddress.getAddress() + " on port " + inetSocketAddress.getPort());
            serverRunning = true;
            selector = Selector.open();
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.bind(inetSocketAddress);
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            serverThread = new Thread(this);
            serverThread.setPriority(Thread.MAX_PRIORITY);
            serverThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*Stops the server*/
    public final void shutdown() {
        if (!serverRunning) {
            return;
        }
        try {
            logger.log(Level.INFO, "Server stopping listening on port " + inetSocketAddress.getPort());
            serverRunning = false;
            if (serverSocketChannel.isOpen()) {
                serverSocketChannel.close();
                serverSocketChannel.socket().close();
            }
            serverThread.join();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*Restarts the server without closing the main server thread.*/
    public final void restart() {
        logger.log(Level.INFO, "Server is restarting");
        try {
            serverRunning = false;
            serverSocketChannel.close();
            serverThread.join();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        serverSocketChannel = null;
        selector = null;
        serverThread = null;
        start();
    }

    /*The main server loop*/
    @Override
    public void run() {
        while (serverRunning) {
            try {
                selector.selectNow();

                for (Iterator<SelectionKey> it = selector.selectedKeys().iterator(); it.hasNext(); ) {
                    SelectionKey key = it.next();

                    //Remove the most recently selected key from the iterator, which is the above selected key.
                    it.remove();

                    if (!key.isValid()) {
                        ConnectionHandler.getInstance().channelInvalid(key);
                        continue;
                    } else if (key.isValid() && !key.equals(lastRemovedKeyTracker)) {

                        if (key.isAcceptable()) {
                            ConnectionHandler.getInstance().channelAccept(key);
                        }

                        if (key.isReadable()) {
                            System.out.println("key is readable");
                            ConnectionHandler.getInstance().channelRead(key);
                        }

                        if (key.isWritable()) {
                            ConnectionHandler.getInstance().channelWrite(key);
                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
