package net;


import game.entity.player.Player;
import game.entity.player.PlayerAttributes;
import game.entity.player.PlayerCredentials;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Created by Dale on 23/01/2015.
 */
public class ClientSession {
    private final InetAddress ipAddress;
    private final SelectionKey selectionKey;
    private final SocketChannel socketChannel;
    private final ByteBuffer inBuffer;
    private final ByteBuffer outBuffer;
    private final Player player;


    public ClientSession(SelectionKey key, InetAddress address) {
        ipAddress = address;
        inBuffer = ByteBuffer.allocateDirect(9048);
        outBuffer = ByteBuffer.allocateDirect(5048);
        socketChannel = (SocketChannel) key.channel();
        selectionKey = key;
        player = new Player(this, new PlayerCredentials(),new PlayerAttributes());
    }


    public void write(ByteBuffer buffer) {

        buffer.flip();

    }

    public void writeOutBuffer() {

        outBuffer.flip();

        if (!socketChannel.isConnected()) {
            disconnect();
        }
        try {

            socketChannel.write(outBuffer);

        } catch (Exception e) {
            e.printStackTrace();
        }

        outBuffer.clear();
    }

    public void disconnect() {
        ConnectionHandler.getInstance().removeClient(this.ipAddress);
        this.inBuffer.clear();
        this.outBuffer.clear();
        try {
            this.socketChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Getter for property 'selectionKey'.
     *
     * @return Value for property 'selectionKey'.
     */
    public SelectionKey getSelectionKey() {
        return selectionKey;
    }

    /**
     * Getter for property 'socketChannel'.
     *
     * @return Value for property 'socketChannel'.
     */
    public SocketChannel getSocketChannel() {
        return socketChannel;
    }

    /**
     * Getter for property 'inBuffer'.
     *
     * @return Value for property 'inBuffer'.
     */
    public ByteBuffer getInBuffer() {
        return inBuffer;
    }

    /**
     * Getter for property 'outBuffer'.
     *
     * @return Value for property 'outBuffer'.
     */
    public ByteBuffer getOutBuffer() {
        return outBuffer;
    }

    /**
     * Getter for property 'ipAddress'.
     *
     * @return Value for property 'ipAddress'.
     */
    public InetAddress getIpAddress() {
        return ipAddress;
    }

    /**
     * Getter for property 'player'.
     *
     * @return Value for property 'player'.
     */
    public Player getPlayer() {
        return player;
    }
}
