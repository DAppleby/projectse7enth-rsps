package net;

import net.packets.IPacketImpl;
import net.packets.LoginPacket;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Dale on 23/01/2015.
 */
public class ConnectionHandler {

    private static ConnectionHandler connectionHandler = null;
    private final int MAX_CONNECTIONS = 500;
    private final int CONNECTIONS_PER_IP = 2;


    /*Creates a thread pool that creates new threads as needed, but will reuse previously constructed threads when they are available.
    These pools will typically improve the performance of programs that execute many short-lived asynchronous tasks.*/
    ExecutorService readPacketsService = Executors.newCachedThreadPool();

    //Concurrent HashMap Accessed by multiple threads used to hold the currently connected clients.
    private ConcurrentHashMap<InetAddress, SelectionKey> connectedClients = new ConcurrentHashMap<>();

    private IPacketImpl[] packets = {
            new LoginPacket()
    };


    /*Singleton class used for accepting, reading and writing client connections*/
    public static ConnectionHandler getInstance() {
        if (connectionHandler == null) {
            return connectionHandler = new ConnectionHandler();
        }
        return connectionHandler;
    }

    public static ConnectionHandler getConnectionHandler() {
        return connectionHandler;
    }

    public void channelAccept(SelectionKey key) {
        if (!key.isValid()) {
            return;
        }
        //Get the server socket channel from the key
        ServerSocketChannel channel = (ServerSocketChannel) key.channel();

        try {
            //Accept the client
            SocketChannel clientChannel = channel.accept();
            //Get the clients INetAddress
            InetAddress address = channel.socket().getInetAddress();

            //If the address is null then disconnect the client
            if (address == null) {
                ((ClientSession) key.attachment()).disconnect();
            }
            //Count the total number of connections with this IP that are currently connected
            int thisIPSTotalConnections = (int) connectedClients.keySet().stream().filter(Objects::nonNull).map(e -> e.equals(address)).count();

            /*Enforce the connection rules, whereby total connections
            cannot exceed max connetions and this ips connects cant exceed connections per IP.*/
            if (connectedClients.size() == MAX_CONNECTIONS ||
                    thisIPSTotalConnections == CONNECTIONS_PER_IP || clientChannel == null) {

                key.cancel();
                if (clientChannel != null)
                    clientChannel.close();

                return;
            }
            /*Configure the clientChannel to be non-blocking*/
            clientChannel.configureBlocking(false);

            /*Register the clients channel with the selector, and set it for reading*/
            SelectionKey registeredKey = clientChannel.register(key.selector(), SelectionKey.OP_READ);

            /*Attach a newly created client session to the key*/
            registeredKey.attach(new ClientSession(registeredKey, address));

            /*Add the newly connected client to the connectClients list*/
            connectedClients.put(address, registeredKey);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void channelRead(SelectionKey key) throws IOException {

        //Get the channel and the session from the selection key
        SocketChannel channel = (SocketChannel) key.channel();
        ClientSession session = (ClientSession) key.attachment();

        System.out.println("reading");
        try {
            int read;
            //Check to make sure the the read data is not end of stream and read the data into the clients buffer
            // If read returns -1  it is its highly likely the client has disconnected.
            try {
                if ((read = channel.read(session.getInBuffer())) == -1) {
                    session.disconnect();
                    return;
                }
                System.out.println("read = " + read);
            }catch (IOException e){
                session.disconnect();
            }



            //Flip the buffer setting the position to 0 and the limit to the current position
            session.getInBuffer().flip();

            //Read the first byte of data (The packets opcode)
            int opCode = session.getInBuffer().get();

            System.out.println("Opcode:" + opCode + " has been received from the client");
            //Submit this whole thing to an executor service to handle.

            readPacketsService.execute(() -> {
                for (IPacketImpl impl : packets) {
                    Arrays.stream(impl.getOpCode()).forEach(e -> {
                        if (e == opCode) {
                            impl.handle(key, opCode);
                        }
                    });
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        session.getInBuffer().clear();
    }

    public void channelWrite(SelectionKey key) {
        ClientSession session = (ClientSession) key.attachment();
        SocketChannel channel = (SocketChannel) key.channel();

        //Check to make sure the channel is connected
        if (!channel.isConnected()) {
            return;
        }

        //Flip the buffer and set the position to 0 and the limit to the current position
        session.getOutBuffer().flip();

        //Init two integers, one for holding the currently written bytes and one to hold the remaining bytes in the buffer.
        int written = 0;
        int remaining = session.getOutBuffer().remaining();


        try {
            //Attempt to write the bytes to the client.
            written = channel.write(session.getOutBuffer());
            System.out.println("wrote out " + written);

            //Check to see if all the bytes were written out
            if (written < remaining) {
                //if not try and write out the remaining bytes.
                channel.write(session.getOutBuffer());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        //Clear the buffer
        session.getOutBuffer().clear();
    }

    public void channelInvalid(SelectionKey key) {

        try {
            ClientSession session = (ClientSession) key.attachment();

            if (session != null) {
                //Disconnect the session as the key is invalid
                session.disconnect();
            } else {
                //Maybe the client session for this key hasn't yet been constructed and thus returns null
                //So disconnect the session manually.
                SocketChannel channel = (SocketChannel) key.channel();
                channel.close();
                key.cancel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void removeClient(InetAddress iNetAddress) {
        if (!(connectedClients.get(iNetAddress) == null)) {
            connectedClients.remove(iNetAddress);
        }
    }

    public int getMAX_CONNECTIONS() {
        return MAX_CONNECTIONS;
    }

    public int getCONNECTIONS_PER_IP() {
        return CONNECTIONS_PER_IP;
    }

    public ExecutorService getReadPacketsService() {
        return readPacketsService;
    }

    public ConcurrentHashMap<InetAddress, SelectionKey> getConnectedClients() {
        return connectedClients;
    }

    public IPacketImpl[] getPackets() {
        return Arrays.copyOf(packets, packets.length);

    }
}
