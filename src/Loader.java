import game.GameEngine;
import game.task.combat.TeleBlockTimerTask;
import net.ServerEngine;
import plugins.api.PluginManager;
import plugins.security.PluginSecurityPolicy;
import java.io.File;
import java.net.InetSocketAddress;
import java.net.URL;
import java.security.*;

/**
 * Created by Dale on 23/01/2015.
 */
public class Loader {

    public enum BootStrap{
        SERVER{
            @Override
            protected void load(){
                new ServerEngine(new InetSocketAddress("localhost", 43594)).start();
            }
        },
        GAMEENGINE{
            @Override
            protected void load(){
                try {
                    GameEngine.getInstance();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        },
        PLUGINS{
            @Override
            protected void load(){
                try {
                    AccessController.doPrivileged((PrivilegedExceptionAction<Object>) ()-> {
                        new PluginManager(new URL[]{new File(PluginManager.PLUGINS_DIR).toURI().toURL()}).loadAllPlugins();
                        return null;
                    });
                } catch (PrivilegedActionException e) {
                    e.printStackTrace();
                }

            }
        },
        SECURITY{
            @Override
            protected void load(){
                Policy.setPolicy(new PluginSecurityPolicy());
                System.setSecurityManager(new SecurityManager());
            }
        };

        protected abstract void load();
    }


    private static void bootServer() {
        try {
            BootStrap.SERVER.load();
            BootStrap.GAMEENGINE.load();
            BootStrap.SECURITY.load();
            BootStrap.PLUGINS.load();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
            bootServer();
    }
}
